/**
 * out.c
 * 
 * LED and segment display output implementation
 */
 
 #include "out.h"

#include <stdio.h>

// RIOT
#include "periph/gpio.h"

// custom
#include "hwio.h"

// private function definitions
void display( int number );

// digit bit patterns for segment display output

#ifdef OUT_SEG

#define DIGIT_ERROR (10)

const uint8_t DIGIT_BITS[12] = {
	0x3f,
	0x0c,
	0x5b,
	0x5d,
	0x6c,
	0x75,
	0x77,
	0x1c,
	0x7f,
	0x7d,
	0x40, // DIGIT_ERROR: '-'
};
#endif

// PUBLIC interface

void io_show( int result ) 
{
	printf( "hwio: show %d\n", result );
	display( result );
}

// PRIVATE functions

bool set_pin( output_t pin, bool on )
{
	//printf( "set led %d to %d\n", led, on );
	if( pin < NUM_OUTPUT_PINS )
	{
		if( on )
			gpio_set( get_output_pin( pin ) );
		else
			gpio_clear( get_output_pin( pin ) );
	}
	else
	{
		printf( "hwio: ERROR: Output %d not available", pin );
		return false;
	}
	
	return true;
}

bool set_pins( uint32_t bits )
{
	for( output_t pin = 0; pin < NUM_OUTPUT_PINS; pin++ )
	{
		if( !set_pin( pin, bits % 2 == 1 ) )
			return false;
		bits >>= 1;
	}
	return true;
}

// displays the specified number (interpreted as an unsigned int) via LED
// or segment display output
void display( int number )
{
#ifdef OUT_LED
	set_pins( number );
#endif

#ifdef OUT_SEG
	uint8_t digit1;
	uint8_t digit2;
	
	if( number >= 0 )
	{
		digit1 = number % 10;
		digit2 = ( number / 10 ) % 10;
	}
	else
	{
		digit1 = DIGIT_ERROR;
		digit2 = DIGIT_ERROR;
	}
	
	uint16_t bits = ( DIGIT_BITS[digit2] << 7 ) | DIGIT_BITS[digit1];
	set_pins( bits );
#endif
}
