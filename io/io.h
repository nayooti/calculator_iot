#ifndef IO_H
#define IO_H

#include "../calc/calc.h"

int io_init( void);

void io_read( Op_type *operation, int *operand1, int *operand2);

void io_show( int result); 

#endif
