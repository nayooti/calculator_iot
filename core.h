/**
* core.h
* 
* core module declarations
*/

#ifndef CORE_H
#define CORE_H

#include "calc/calc.h"
#include "net/net.h"

/* delegation function, called from net module */
int core_do_operation( Op_type operation, int operand1, int operand2);

#endif
