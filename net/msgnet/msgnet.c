/**
* msgnet.c
* 
* Networking submodule udp-only implementation
*/

#include <stdio.h>
#include "net/gnrc.h"
#include "net/gnrc/ipv6.h"
#include "net/gnrc/udp.h"

#include "../../core.h"
#include "../net.h"
#include "../net_commands.h"
#include "../net_submodule.h"


void udp_send( char *addr_str, /*char *port_str,*/ char *data)
{
    uint16_t port = APP_PORT;
    ipv6_addr_t addr;
    
    /* parse destination address */
    if (ipv6_addr_from_str(&addr, addr_str) == NULL) {
        puts("msgnet: Error: unable to parse destination address");
        puts(addr_str);
        return;
    }

	gnrc_pktsnip_t *payload, *udp, *ip;
	/* allocate payload */
	payload = gnrc_pktbuf_add(NULL, data, strlen(data), GNRC_NETTYPE_UNDEF);
	if (payload == NULL) {
		puts("msgnet: Error: unable to copy data to packet buffer");
		return;
	}
	/* allocate UDP header, set source port := destination port */
	udp = gnrc_udp_hdr_build(payload, port, port);
	if (udp == NULL) {
		puts("msgnet: Error: unable to allocate UDP header");
		gnrc_pktbuf_release(payload);
		return;
	}
	
	/* allocate IPv6 header */
	ipv6_addr_t src;
    ipv6_addr_from_str(&src, dev_unicast_addr);
	ip = gnrc_ipv6_hdr_build(udp, &src, &addr);
	if (ip == NULL) {
		puts("msgnet: Error: unable to allocate IPv6 header");
		gnrc_pktbuf_release(udp);
		return;
	}
	/* send packet */
	if (!gnrc_netapi_dispatch_send(GNRC_NETTYPE_UDP, GNRC_NETREG_DEMUX_CTX_ALL, ip)) {
		puts("msgnet: Error: unable to locate UDP thread");
		gnrc_pktbuf_release(ip);
		return;
	}
	printf("msgnet: sent %d bytes (src: [%s]:%d) to [%s]:%d\n", payload->size,
		   dev_unicast_addr, port, addr_str, port);
}


void netsub_reply( gnrc_pktsnip_t *pktsnip, char *response, int len) {

	(void) len;
	
	ipv6_hdr_t *iphdr = (ipv6_hdr_t*) pktsnip->next->next->data;
	char ipv6_addr_src[IPV6_ADDR_MAX_STR_LEN];
	ipv6_addr_to_str(ipv6_addr_src, &iphdr->src, IPV6_ADDR_MAX_STR_LEN);

	udp_send( ipv6_addr_src, response);
}

int netsub_get_payload( char *buf, gnrc_pktsnip_t *pktsnip) {
	
	strncpy(buf, pktsnip->data, pktsnip->size);
	buf[pktsnip->size] = '\0';
	return 0;
}

int netsub_is_response( gnrc_pktsnip_t *pktsnip) {
	
	return ((char*)pktsnip->data)[0] == '=';
}

int netsub_is_request( gnrc_pktsnip_t *pktsnip) {
	
	return ((char*)pktsnip->data)[0] == '?';
}

void netsub_sendto( char* dst_addr, char* data, int len) {
	
	(void) len;
	udp_send( dst_addr, data);
}
