/**
* net_commands.h
* 
* Networking utility declarations
*/

#ifndef NET_COMMANDS
#define NET_COMMANDS

#define PREFIX_LOCAL_UNICAST		"fe80::"
#define PREFIX_GLOBAL_UNICAST		"2001:db8::"
#define PREFIX_LOCAL_MULTICAST 		"ff02::1:ff00:"
#define PREFIX_GLOBAL_MULTICAST 	"ff0e::1:ff00:"

/* RPL-root ip address */
#define RPL_ROOT_GLOBAL_ADDR (PREFIX_GLOBAL_UNICAST "1")

/* operation specific ip address */
#define ADDR_OP(prefix, op) (	(op==OP_ADD) ? prefix "10" : \
								(op==OP_SUB) ? prefix "20" : \
								(op==OP_MUL) ? prefix "30" : \
								(op==OP_DIV) ? prefix "40" : "" )

#ifdef NORPL
	#define ANYCAST_ADDR(op) ( ADDR_OP( PREFIX_LOCAL_UNICAST, op) )
#else
	#define ANYCAST_ADDR(op) ( ADDR_OP( PREFIX_GLOBAL_UNICAST, op) )
#endif

#ifdef NORPL
	#define MULTICAST_ADDR(op) ( ADDR_OP( PREFIX_LOCAL_MULTICAST, op) )
#else
	#define MULTICAST_ADDR(op) ( ADDR_OP( PREFIX_GLOBAL_MULTICAST, op) )
#endif

extern int _netif_config(int argc, char **argv);
extern int _gnrc_rpl(int argc, char **argv);

/* initialize network utilities and get own ip address */
void cmd_init( void);

/* get own local unicast address */
ipv6_addr_t * cmd_get_unicast_addr( void);

#ifndef NORPL

extern int _fib_route_handler(int argc, char **argv);

/* enlist rpl root address */
void cmd_add_root_global_addr( void);

/* initialize rpl */
void cmd_init_rpl( void);

/* set rpl root address */
void cmd_rpl_root( void);

#endif

/* enlist anycast address */
void cmd_add_anycast_addr( char*);

/* enlist multicast address */
void cmd_add_multicast_addr( char*);

/* add a route to the fib */
void cmd_add_route( char *dst, char *next);

#endif
