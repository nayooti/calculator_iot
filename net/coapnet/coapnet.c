/**
* coapnet.c
* 
* Networking submodule implementation with coap
*/

#include <stdio.h>
#include <coap.h>
#include <random.h>
#include "net/gnrc.h"
#include "net/gnrc/ipv6.h"
#include "net/gnrc/udp.h"
#include "net/conn/udp.h"
#include "net/af.h"
#include "cbor.h"

#include "../net.h"
#include "../net_submodule.h"


static uint8_t _udp_buf[512]; /* udp send buffer (max udp payload size) */
static uint8_t scratch_raw[1024]; /* microcoap scratch buffer */
static coap_rw_buffer_t scratch_buf = { scratch_raw, sizeof(scratch_raw) };

const coap_endpoint_t endpoints[] =
{
    /* marks the end of the endpoints array: */
    { (coap_method_t)0, NULL, NULL, NULL }
};

// übernommen und verändert von: https://github.com/fridalufa/smart-environment/blob/master/shared/coap_client.c
void coap_client_send(ipv6_addr_t* target, coap_method_t method, coap_msgtype_t type, char* endpoint, char* payload, size_t payloadSize, coap_content_type_t ct)
{
    int endpointLen = strlen(endpoint);
    char uri[endpointLen + 1];
    strncpy(uri, endpoint, endpointLen);
    uri[endpointLen] = '\0';

    // count the amount of parts of the endpoint URI
    int parts = 1;
    for (uint8_t i = 0; i < strlen(uri); i++) {
        if (uri[i] == '/') {
            parts++;
        }
    }

    // if there is a payload add another option for the content type
    int numOptions = parts;
    if (payload != NULL) {
        numOptions += 1;
    }

    coap_option_t opts[numOptions];

    // construct option array containg the parts of the endpoint URI

    char* token = strtok(uri, "/");
    int i = 0;
    while (token != NULL) {

        coap_buffer_t optBuf = {
            (uint8_t*) token,
            strlen(token)
        };
        coap_option_t opt = {0};
        opt.num = COAP_OPTION_URI_PATH;
        opt.buf = optBuf;

        opts[i] = opt;

        i++;
        token = strtok(NULL, "/");
    }

    // add content type option if necessary
    if (payload != NULL) {

        coap_buffer_t optBuf = {
            (uint8_t*)& ct,
            2
        };
        coap_option_t opt = {0};
        opt.num = COAP_OPTION_CONTENT_FORMAT;
        opt.buf = optBuf;

        opts[i] = opt;
    }

    // check if message type is allowed (either confirmable or non-confirmable)
    if (type < 0 || type > 3) {
        type = COAP_TYPE_CON;
    } else if (type != COAP_TYPE_CON && type != COAP_TYPE_NONCON) {
        printf("Invalid message type. Valid types for requests are confirmable and non-confirmable.");
    }


    // construct the packet
    coap_packet_t pkt;

    // begin with constructing the header
    pkt.hdr = (coap_header_t) {
        1,             // CoAP protocol version
        type,          // message type (either confirmable or non-confirmable)
        0,             // token length (currently zero since we do not support tokens at the moment)
        method,        // method to use
        {0, 0}         // id placeholder, actual id gets generated below
    };

    // generate a random 16 bit message id
    uint32_t r = random_uint32_range(0, 0xFFFF);
    uint8_t id[] = {(r >> 8) & 0xFF , r & 0xFF};
    memcpy(pkt.hdr.id, id, 2);

    // add options
    pkt.numopts = numOptions;
    memcpy(pkt.opts, opts , numOptions * sizeof(opts));

    // add the payload if there is one
    if (payload != NULL) {
        coap_buffer_t payloadBuf = {
            (uint8_t*) payload,
            payloadSize
        };
        pkt.payload = payloadBuf;
    }

    coap_dumpPacket(&pkt);

    // send the paket over UDP
    int rc = 0;
    //~ static uint8_t _udp_buf[512] = {0};
    //~ memset(_udp_buf, 0, 512);
    size_t pktlen = sizeof(_udp_buf);
    if ((rc = coap_build(_udp_buf, &pktlen, &pkt)) != 0) {
        printf("coapnet: coap_build failed rc=%d\n", rc);
    } else {
	
		ipv6_addr_t srcip;
		ipv6_addr_from_str(&srcip, dev_unicast_addr);
	
        rc = conn_udp_sendto(_udp_buf, pktlen, srcip.u8, 16, target, 16, AF_INET6, CLIENT_PORT , APP_PORT);
        if (rc < 0) {
            printf("coapnet: Error sending CoAP reply via udp; %d\n", rc);
        }
        else {
			char ipv6_addr_dst[IPV6_ADDR_MAX_STR_LEN];
			ipv6_addr_to_str(ipv6_addr_dst, target, IPV6_ADDR_MAX_STR_LEN);
			printf("coapnet: sent %d bytes (src: [%s]:%d) to [%s]:%d\n",
				pktlen, dev_unicast_addr, CLIENT_PORT, ipv6_addr_dst, APP_PORT);
		}
    }
}

void netsub_reply( gnrc_pktsnip_t *pktsnip, char *response, int len) {

	coap_packet_t coap_pkt;
	coap_packet_t rsppkt;
        
	coap_parse(&coap_pkt, (uint8_t*)pktsnip->data, pktsnip->size);
	
	/* handle CoAP request */
	//~ coap_handle_req(&scratch_buf, &coap_pkt, &rsppkt);
	
	coap_make_response( &scratch_buf, &rsppkt, (uint8_t*)response, len,
		coap_pkt.hdr.id[0], coap_pkt.hdr.id[1], &coap_pkt.tok, 
		COAP_RSPCODE_CONTENT, COAP_CONTENTTYPE_TEXT_PLAIN);
		
	/* add option to identify calculation packets */
	coap_buffer_t optBuf = {
        (uint8_t*) "calc",
        strlen("calc")
    };
    coap_option_t opt = {0};
    opt.num = COAP_OPTION_URI_PATH;
    opt.buf = optBuf;
    rsppkt.numopts = 1;
    rsppkt.opts[0] = opt;
			
	/* build reply */
	int rc;
	//~ static uint8_t _udp_buf[512] = {0};
	size_t rsplen = sizeof(_udp_buf);
	if ((rc = coap_build(_udp_buf, &rsplen, &rsppkt)) != 0) {
		printf("coap_build failed rc=%d\n", rc);
	} else {
		pktsnip = pktsnip->next;
		udp_hdr_t *udp_hdr = (udp_hdr_t *) pktsnip->data;
		
		pktsnip = pktsnip->next;
		ipv6_hdr_t *ip_hdr = (ipv6_hdr_t*) pktsnip->data;
		ipv6_addr_t *ip_addr = &ip_hdr->src;
		
		uint32_t port = (uint32_t)byteorder_ntohs(udp_hdr->src_port);
		
		ipv6_addr_t srcip;
		ipv6_addr_from_str(&srcip, dev_unicast_addr);

		/* send reply via UDP connection api */
		rc = conn_udp_sendto(_udp_buf, rsplen, srcip.u8, 16, ip_addr->u8, 16, AF_INET6, APP_PORT, port);
		if (rc < 0) {
			printf("coapnet: Error sending CoAP reply via udp; %d\n", rc);
		}
		else {
			char ipv6_addr_dst[IPV6_ADDR_MAX_STR_LEN];
			ipv6_addr_to_str(ipv6_addr_dst, ip_addr, IPV6_ADDR_MAX_STR_LEN);
			printf("coapnet: response: sent %d bytes (src: [%s]:%d) to [%s]:%d\n",rsplen,
				dev_unicast_addr, APP_PORT, ipv6_addr_dst, (int)port);
		}
	}
}


int netsub_get_payload( char *buf, gnrc_pktsnip_t *pktsnip) {
	
	coap_packet_t coap_pkt;
    int rc;
	rc = coap_parse(&coap_pkt, (uint8_t*)pktsnip->data, pktsnip->size);

	if( rc != 0) {
	    printf("Bad packet rc=%d\n", rc);
	    return -1;
	}
	
	#ifdef NOCBOR
	coap_buffer_to_string(buf, MAX_PAYLOAD, &coap_pkt.payload);
	#else
	memcpy(buf, (char*) coap_pkt.payload.p, coap_pkt.payload.len);
	#endif
	
	return coap_pkt.payload.len;
}

int netsub_is_response( gnrc_pktsnip_t *pktsnip) {
	
	coap_packet_t coap_pkt;
	coap_parse(&coap_pkt, (uint8_t*)pktsnip->data, pktsnip->size);
	return coap_pkt.numopts>0 
			&& strncmp((char*)coap_pkt.opts[0].buf.p, "calc", strlen("calc")) == 0 
			&& coap_pkt.hdr.t == COAP_TYPE_ACK 
			&& coap_pkt.hdr.code == MAKE_RSPCODE(2, 5);
}

int netsub_is_request( gnrc_pktsnip_t *pktsnip) {
	
	coap_packet_t coap_pkt;
	coap_parse(&coap_pkt, (uint8_t*)pktsnip->data, pktsnip->size);
	return coap_pkt.numopts>0 
			&& strncmp((char*)coap_pkt.opts[0].buf.p, "calc", strlen("calc")) == 0 
			&& coap_pkt.hdr.t != COAP_TYPE_ACK;
}

void netsub_sendto( char* dst_addr, char* data, int len, coap_method_t method, char *uri) {
	
    ipv6_addr_t target;
    ipv6_addr_from_str(&target, dst_addr);
    coap_client_send(&target, method, COAP_TYPE_CON, uri, data, len, COAP_CONTENTTYPE_TEXT_PLAIN);
}
