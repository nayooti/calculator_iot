/**
* net_submodule.h
* 
* Networking submodule declarations
*/

#ifndef NET_SUBMODULE
#define NET_SUBMODULE

/* return content and length of received packet */
int netsub_get_payload( char *buf, gnrc_pktsnip_t *pktsnip);

/* return true if packet is a calculation response */
int netsub_is_response( gnrc_pktsnip_t *pktsnip);

/* return true if packet is a calculation request */
int netsub_is_request( gnrc_pktsnip_t *pktsnip);

#ifdef NOCOAP
/* send data to destination board */
void netsub_sendto( char* dst_addr, char* data, int len);
#else
#include <coap.h>
/* send data to destination board with coap */
void netsub_sendto( char* dst_addr, char* data, int len, coap_method_t method, char *uri);
#endif

/* reply to packet source with a response */
void netsub_reply( gnrc_pktsnip_t *pktsnip, char *response, int len);

#endif
