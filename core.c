/**
* core.h
* 
* core module implementation
*/

#include <stdio.h>
#include "shell.h"
#include "ps.h"

#include "core.h"
#include "io/io.h"
#include "net/net.h"
#include "calc/calc.h"


int main(void)
{
    puts("Welcome to Calculator-IoT!");

	net_init();

	/* show threads with their stack sizes */
    ps();

#if (defined(IO_SW) || defined (IO_HW) || defined (IO_DUMMY))
    io_init();

	int result;
	Op_type operation = OP_MUL;
	int operand1 = 0, operand2 = 0;

    /* main loop of the io thread */
	while(1) {

		puts("core: Start der Eingabe:");
		io_read( &operation, &operand1, &operand2);
		printf("core: Ende der Eingabe: %d %d %d\n", operation, operand1, operand2);

		result = -1;
		if( calc_is_supported( operation)) {
			puts("core: Operation lokal ausgeführt");
			result = calc_do_operation( operation, operand1, operand2);
		}
		else {
			puts("core: Operation lokal nicht unterstützt");
			result = net_ask_network( operation, operand1, operand2);
		}

		puts("core: Ausgabe des Resultats");
		io_show( result);
	}

#endif // with io

    return 0;
}

int core_do_operation( Op_type operation, int operand1, int operand2) {

	return calc_do_operation( operation, operand1, operand2);
}


