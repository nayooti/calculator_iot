
zur Architektur
---

- ein Hauptmodul core, das die drei Module io, net und calc benutzt
- entweder IO über Schalter und LEDs bzw. Ziffernblock oder über Terminal, Schnittstellen müssen implementiert werden
- falls nicht fähig eine Eingabe zu bearbeiten, sendet ein Gerät eine Anfrage an ein oder mehrere andere Geräte
- Compiler-Flags wie IO_HW, CANADD bestimmen womit ein Gerät ausgestattet ist und welche Rechenoperation es unterstützt
- zwei Threads: IO- und Netzwerkbehandlung
